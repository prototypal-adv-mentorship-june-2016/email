import Ember from 'ember';
import QUnit from 'qunit';

QUnit.assert.contains = function(selector, text, message) {
  var elements = find(selector);
  var regex = new RegExp(escapeForRegex(text) + '($|\\W)', 'gm');
  var result = false;
  message = `${(message || '')} - At least one element ${selector} containing "${text}" should exist.`;

  if (elements.length === 1) {
    var resultText = textOrVal(elements);
    result = regex.test(resultText);
    this.push(result, resultText, text, message);
  } else {
    elements.each(function() {
      if (regex.test(textOrVal($(this)))) {
        result = true;
      }
    });
    this.push(result, result, true, message);
  }
};

QUnit.assert.containsExactly = function(selector, text, message = '') {
  let elements = find(selector);
  if (elements.length !== 1) {
    throw new Error(`One element was expected with selector ${selector} but ${elements.length} were found`);
  }
  let result = (Ember.$(elements[0]).text().trim().replace(/\s+/gm, '') === text.trim().replace(/\s+/gm, ''));
  message = `${message} - ${selector} should contain ${text}`;
  this.push(result, Ember.$(elements[0]).text(), text, message);
};

function textOrVal(element) {
  if (element.is('input') || element.is('textarea')) {
    return element.val();
  } else {
    return element.text();
  }
}

function escapeForRegex(str) {
  return str.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&');
}
