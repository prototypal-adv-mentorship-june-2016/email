import resolver from './helpers/resolver';
import './helpers/flash-message';

import {
  setResolver
} from 'ember-qunit';

import './helpers/qunit-assertions';

setResolver(resolver);
