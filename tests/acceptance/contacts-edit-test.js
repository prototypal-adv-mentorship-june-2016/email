import Ember from 'ember';
import { test } from 'qunit';
import moduleForAcceptance from 'email/tests/helpers/module-for-acceptance';
import Mirage from 'ember-cli-mirage';

moduleForAcceptance('Acceptance | contacts edit');

test('Edit contact displays contact current info', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith'
  });

  server.create('contact-email', { contactId: contact.id, address: 'smith@example.com', primary: true });
  server.create('contact-email', { contactId: contact.id, address: 'smith@example.org' });
  server.create('contact-email', { contactId: contact.id, address: 'asmith@example.edu' });
  server.create('contact-phone', { contactId: contact.id, number: '+15555555252', primary: true });
  server.create('contact-phone', { contactId: contact.id, number: '+15555550002' });

  visitEditContact(contact);

  andThen(function() {
    assert.equal(currentURL(), `/contacts/${contact.id}`);
    assert.contains('.test-contact-name input', 'Ann Smith', 'Contact name is displayed');
    assert.contains('.test-contact-phone input', '+15555555252', 'Contact primary phone is displayed');
    assert.contains('.test-contact-phone input', '+15555550002', 'Contact alternate phone is displayed');
    assert.contains('.test-contact-email input', 'smith@example.com', 'Contact primary email is displayed');
    assert.contains('.test-contact-email input', 'smith@example.org', 'Contact alternate email is displayed');
    assert.contains('.test-contact-email input', 'asmith@example.edu', 'Contact work email is displayed');
  });
});

test('Edit contact allows editing contact info', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith'
  });
  let primaryEmail = server.create('contact-email', { contactId: contact.id, address: 'smith@example.com', primary: true });
  server.create('contact-email', { contactId: contact.id, address: 'smith@example.org' });
  server.create('contact-email', { contactId: contact.id, address: 'asmith@example.edu' });
  let primaryPhone = server.create('contact-phone', { contactId: contact.id, number: '+15555555252', primary: true });
  server.create('contact-phone', { contactId: contact.id, number: '+15555550002' });

  visitEditContact(contact);
  fillIn('.test-contact-name input', 'Jane Smith');
  fillIn('.test-contact-phone:eq(0) input', '+15555555312');
  fillIn('.test-contact-email:eq(0) input', 'smith@example.net');
  click('.test-save-contact');

  andThen(function() {
    contact.reload();
    assert.equal(contact.name, 'Jane Smith', 'Contact name is updated');

    primaryPhone.reload();
    assert.equal(primaryPhone.number, '+15555555312', 'Primary phone is updated');

    primaryEmail.reload();
    assert.equal(primaryEmail.address, 'smith@example.net', 'Primary email is updated');
  });
});

test('Edit contact handles server error when saving contact', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith'
  });

  let primaryEmail = server.create('contact-email', { contactId: contact.id, address: 'smith@example.com', primary: true });
  let primaryPhone = server.create('contact-phone', { contactId: contact.id, number: '+15555555252', primary: true });

  server.patch('/contacts/:id', function() {
    return new Mirage.Response(500);
  });

  visitEditContact(contact);
  fillIn('.test-contact-name input', 'Jane Smith');
  click('.test-save-contact');

  andThen(function() {
    contact.reload();
    assert.equal(contact.name, 'Ann Smith', 'Contact name is NOT updated');

    primaryPhone.reload();
    assert.equal(primaryPhone.number, '+15555555252', 'Primary phone is NOT updated');

    primaryEmail.reload();
    assert.equal(primaryEmail.address, 'smith@example.com', 'Primary email is NOT updated');

    assert.contains('.alert-danger', 'There was an error saving this contact. Please try again.');
  });
});

test('Edit contact handles server error when saving contact phone', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith'
  });
  let primaryEmail = server.create('contact-email', { contactId: contact.id, address: 'smith@example.com', primary: true });
  let primaryPhone = server.create('contact-phone', { contactId: contact.id, number: '+15555555252', primary: true });
  let alternatePhone = server.create('contact-phone', { contactId: contact.id, number: '+15555551234' });

  server.patch(`/contact-phones/${primaryPhone.id}`, function() {
    return new Mirage.Response(500);
  });

  visitEditContact(contact);
  fillIn('.test-contact-name input', 'Jane Smith');
  fillIn('.test-contact-phone:eq(0) input', '+15555552345');
  fillIn('.test-contact-phone:eq(1) input', '+15555559876');
  fillIn('.test-contact-email input', 'smith@example.net');
  click('.test-save-contact');

  andThen(function() {
    contact.reload();
    assert.equal(contact.name, 'Jane Smith', 'Contact name is updated');

    primaryPhone.reload();
    assert.equal(primaryPhone.number, '+15555555252', 'Primary phone is NOT updated');

    alternatePhone.reload();
    assert.equal(alternatePhone.number, '+15555559876', 'Alternate phone is updated');

    primaryEmail.reload();
    assert.equal(primaryEmail.address, 'smith@example.net', 'Primary email is updated');

    assert.contains('.alert-danger', 'There was an error saving this contact. Please try again.');
  });
});

test('Edit contact handles server error when saving contact email', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith'
  });
  let primaryEmail = server.create('contact-email', { contactId: contact.id, address: 'smith@example.com', primary: true });
  let alternateEmail = server.create('contact-email', { contactId: contact.id, address: 'asmith@example.org' });
  let primaryPhone = server.create('contact-phone', { contactId: contact.id, number: '+15555555252', primary: true });

  server.patch(`/contact-emails/${primaryEmail.id}`, function() {
    return new Mirage.Response(500);
  });

  visitEditContact(contact);
  fillIn('.test-contact-name input', 'Jane Smith');
  fillIn('.test-contact-phone input', '+15555552345');
  fillIn('.test-contact-email:eq(0) input', 'smith@example.net');
  fillIn('.test-contact-email:eq(1) input', 'asmith@example.net');
  click('.test-save-contact');

  andThen(function() {
    contact.reload();
    assert.equal(contact.name, 'Jane Smith', 'Contact name is updated');

    primaryPhone.reload();
    assert.equal(primaryPhone.number, '+15555552345', 'Primary phone is updated');

    primaryEmail.reload();
    assert.equal(primaryEmail.address, 'smith@example.com', 'Primary email is NOT updated');

    alternateEmail.reload();
    assert.equal(alternateEmail.address, 'asmith@example.net', 'Alternate email is updated');

    assert.contains('.alert-danger', 'There was an error saving this contact. Please try again.');
  });
});

test('Edit contact does not allow empty name', function(assert) {
  let contact = server.create('contact');

  visitEditContact(contact);

  andThen(function() {
    assert.equal(find('.test-contact-name-validation').length, 0, 'Contact validation is not showing');
  });

  fillIn('.test-contact-name input', '');

  andThen(function() {
    assert.contains('.test-contact-name-validation', 'Contact name is required');
  });

  click('.test-save-contact');

  andThen(function() {
    contact.reload();
    assert.ok(contact.name !== '', 'Contact is not saved');
  });
});

test('Edit contact allows adding a phone', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith'
  });
  server.create('contact-phone', { number: '+15555555252', primary: true, contactId: contact.id });
  server.create('contact-phone', { number: '+15555550002', contactId: contact.id });

  visitEditContact(contact);
  click('.test-add-phone');
  fillIn('.test-new-phone:eq(0) input', '+15555551234');
  click('.test-save-contact');

  andThen(function() {
    contact.reload();
    assert.equal(contact.contactPhones.models.length, 3, 'Contact has a new phone number');
    assert.equal(Ember.get(contact.contactPhones.models, 'lastObject').number, '+15555551234', 'New number is saved');
  });
});

test('Edit contact allows adding an email', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith'
  });
  server.create('contact-email', { address: 'smith@example.com', primary: true, contactId: contact.id });
  server.create('contact-email', { address: 'asmith@example.org', contactId: contact.id });

  visitEditContact(contact);
  click('.test-add-email');
  fillIn('.test-new-email:eq(0) input', 'ann@example.com');
  click('.test-save-contact');

  andThen(function() {
    contact.reload();
    assert.equal(contact.contactEmails.models.length, 3, 'Contact has a new email');
    assert.equal(Ember.get(contact.contactEmails.models, 'lastObject').address, 'ann@example.com', 'New email is saved');
  });
});

test('Edit contact allows adding multiple emails and/or phones at the same time', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith'
  });

  visitEditContact(contact);
  click('.test-add-email');
  fillIn('.test-new-email:eq(0) input', 'ann@example.com');
  click('.test-add-phone');
  fillIn('.test-new-phone:eq(0) input', '+15555551234');
  click('.test-save-contact');

  andThen(function() {
    contact.reload();
    assert.equal(contact.contactEmails.models.length, 1, 'Contact has a new email');
    assert.equal(Ember.get(contact.contactEmails.models, 'lastObject').address, 'ann@example.com', 'New email is saved');
    assert.equal(contact.contactPhones.models.length, 1, 'Contact has a new phone number');
    assert.equal(Ember.get(contact.contactPhones.models, 'lastObject').number, '+15555551234', 'New number is saved');
  });
});

test('Edit contact allows removing emails', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith'
  });
  server.create('contact-email', { address: 'smith@example.com', primary: true, contactId: contact.id });
  server.create('contact-email', { address: 'asmith@example.org', contactId: contact.id });

  visitEditContact(contact);
  click('.test-remove-email:eq(0)');

  andThen(function() {
    contact.reload();
    assert.equal(contact.contactEmails.models.length, 2, 'Contact still has two emails');
    assert.equal(find('.test-contact-email').length, 1, 'Only one email displaying');
  });

  click('.test-save-contact');

  andThen(function() {
    contact.reload();
    assert.equal(contact.contactEmails.models.length, 1, 'Contact has one fewer email');
  });
});

test('Edit contact allows removing phones', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith'
  });
  server.create('contact-phone', { number: '+15555555252', primary: true, contactId: contact.id });
  server.create('contact-phone', { number: '+15555550002', contactId: contact.id });

  visitEditContact(contact);
  click('.test-remove-phone:eq(0)');

  andThen(function() {
    contact.reload();
    assert.equal(contact.contactPhones.models.length, 2, 'Contact still has two phones');
    assert.equal(find('.test-contact-phone').length, 1, 'Only one phone displaying');
  });

  click('.test-save-contact');

  andThen(function() {
    contact.reload();
    assert.equal(contact.contactPhones.models.length, 1, 'Contact has one fewer phone');
  });
});

test('Save only hits API for dirty models', function(assert) {
  let contact = server.create('contact', {
    name: 'Ann Smith'
  });
  server.create('contact-email', { contactId: contact.id, address: 'smith@example.com', primary: true });
  server.create('contact-email', { contactId: contact.id, address: 'asmith@example.org' });
  server.create('contact-phone', { contactId: contact.id, number: '+15555555252', primary: true });

  let contactPatchCount = 0;
  let contactEmailPatchCount = 0;
  let contactPhonePatchCount = 0;
  server.patch(`/contacts/:id`, function() {
    contactPatchCount++;
  }, 204);

  server.patch(`/contact-emails/:id`, function() {
    contactEmailPatchCount++;
  }, 204);

  server.patch(`/contact-phones/:id`, function() {
    contactPhonePatchCount++;
  }, 204);

  visitEditContact(contact);
  fillIn('.test-contact-phone input', '+15555552345');
  fillIn('.test-contact-email:eq(1) input', 'asmith@example.net');
  click('.test-save-contact');

  andThen(function() {
    assert.equal(contactPatchCount, 0, 'Contact is not updated');
    assert.equal(contactEmailPatchCount, 1, 'Only one email updated');
    assert.equal(contactPhonePatchCount, 1, 'Only one phone updated');
  });
});

function visitEditContact(contact) {
  visit(`/contacts/${contact.id}`);
}
