import { Factory } from 'ember-cli-mirage';

export default Factory.extend({
  address: (i) => `contact-${i}@example.com`,
  primary: false
});
