import { Model, hasMany } from 'ember-cli-mirage';

export default Model.extend({
  contactEmails: hasMany('contact-email'),
  contactPhones: hasMany('contact-phone')
});
