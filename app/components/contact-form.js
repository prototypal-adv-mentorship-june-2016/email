import Ember from 'ember';

const { inject, RSVP, computed } = Ember;

export default Ember.Component.extend({
  contact: null,
  store: inject.service(),
  flashMessages: inject.service(),
  isContactNameValid: computed('contact.name', function() {
    return this.get('contact.name').length > 0;
  }).readOnly(),
  isValid: computed.readOnly('isContactNameValid'),
  actions: {
    saveContact(contact) {
      if (!this.get('isValid')) {
        return;
      }

      return saveAllDirty(contact.get('contactEmails').toArray(), contact.get('contactPhones').toArray(), [contact]).then(() => {
        this.get('flashMessages').success('Contact saved');
      }).catch(() => {
        this.get('flashMessages').danger('There was an error saving this contact. Please try again.');
      });
    },
    addPhone() {
      this.get('store').createRecord('contactPhone', { contact: this.get('contact') });
    },
    addEmail() {
      this.get('store').createRecord('contactEmail', { contact: this.get('contact') });
    },
    removeEmail(email) {
      email.deleteRecord();
    },
    removePhone(phone) {
      phone.deleteRecord();
    }
  }
});

function saveAllDirty(...modelArrs) {
  let allDirty = [].concat(...modelArrs).filterBy('hasDirtyAttributes', true);
  return RSVP.all(allDirty.map((model) => model.save()));
}

